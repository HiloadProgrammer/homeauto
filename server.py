import paho.mqtt.client as mqtt
import logging
import subprocess
from subprocess import call
import os
from pathlib import Path
import ConfigParser
import io
import json
import sys
import traceback
import argparse


config_file_path = "server.conf"
log_path = "server.log"
config = ""

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    logger.info("Connected with result code "+str(rc))

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    logger.info("received message [#%s]%s"%(msg.topic, msg.payload))
    try:
        channel_conf = config.get(msg.topic, msg.payload)
        json_object = json.loads(channel_conf)
        channels = json_object["outgoing"]
        for channel in channels:
            logger.info("publish [#%s]%s"%(channel["channel"],channel["message"]))
            client.publish(channel["channel"],channel["message"])
    except ConfigParser.NoSectionError:
        logger.error("no section section in config file for: %s"%msg.topic)
    except ConfigParser.NoOptionError:
        logger.error("no entry in section %s for: %s"%(msg.topic, msg.payload))
    except KeyError as e:
        logger.error("there is a key error in section '%s' in entry '%s' for the key %s"%(msg.topic, msg.payload, e.args))



def stop_script():
    logger.debug("stop skript")
    quit()

def config_logger():
    global logger
    global log_path
    logger = logging.getLogger('server_log')
    logger.setLevel(logging.DEBUG)
    #logger = logging.basicConfig(filename='server.log',level=logging.DEBUG, format='%(asctime)s %(message)s')
    formatter = logging.Formatter('[%(asctime)s] [%(name)s] [%(levelname)s] - %(message)s')
    fh = logging.FileHandler(log_path)
    fh.setLevel(logging.DEBUG)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    fh.setFormatter(formatter)
    ch.setFormatter(formatter)

    logger.addHandler(ch)
    logger.addHandler(fh)

def check_if_root():
    logger.debug("check if run as root")
    if os.getuid() == 0:
        logger.debug("script is run as root")
    else:
        logger.error("no root permission")
        stop_script()

def config_file_init():
    global config_file_path
    global config

    logger.debug("init config file")
    logger.debug("check if server.conf exists")
    config_file = Path(config_file_path)

    if config_file.is_file():
        logger.debug("config file exists")
    else:
        logger.error("config file does not exists!")
        logger.info("createing config file")
        logger.info("create '%s' file"%config_file_path)
        config = ConfigParser.RawConfigParser()
        config.add_section('server config')
        config.set('server config', 'url', '127.0.0.1')
        config.set('server config', 'port', "1883")
        config.set('server config', 'user', "username")
        config.set('server config', 'docker image', "pascaldevink/rpi-mosquitto")
        config.set('server config', 'docker container name', "mqtt")
        config.set('server config', 'channels', '{"channels":["channel1", "channelN", "channelN1"]}')
        config.add_section('channel1')
        config.set('channel1', 'incomming_message', '\n{\n\t"outgoing":\n\t[\n\t\t{"channel":"channelN", "message":"outgoing_message"},\n\t\t{"channel":"channelN1", "message":"outgoing_message2"}\n\t]\n}')
        config.set('channel1', 'incomming_message2', '\n{\n\t"outgoing":\n\t[\n\t\t{"channel":"channelN", "message":"outgoing_message"},\n\t\t{"channel":"channelN1", "message":"outgoing_message2"}\n\t]\n}')
        with open(config_file_path, 'wb') as configfile:
            config.write(configfile)
        logger.info("config file created")
        logger.info("edit the config file to your likes and start the app again")
        stop_script()

    logger.debug("read config file")
    config = ConfigParser.RawConfigParser()
    config.read(config_file_path)

def init_mqtt():
    global config
    logger.debug("init mqtt")
    container_name = config.get('server config', 'docker container name')
    logger.info("check if docker container '%s' is running"%container_name)
    cmd = [ 'sudo', 'docker', 'ps', '-f', 'name=%s'%container_name ]
    output = subprocess.Popen( cmd, stdout=subprocess.PIPE ).communicate()[0]
    words = []
    for word in output.split(" "):
        tmp = word
        tmp.replace(" ","").replace("\t","").replace("\n","")
        if tmp is not "":
            words.append(word)
    if len(words)<9:
        logger.warn("there is no docker running")
        logger.info("check if container exits")
        cmd = [ 'sudo', 'docker', 'ps', '-a', '-f', 'name=%s'%container_name ]
        output = subprocess.Popen( cmd, stdout=subprocess.PIPE ).communicate()[0]
        words = []
        for word in output.split(" "):
            tmp = word
            tmp.replace(" ","").replace("\t","").replace("\n","")
            if tmp is not "":
                words.append(word)
        if len(words)<9:
            logger.warn("there is no docker container with name: '%s'"%container_name)
            logger.debug("check if mqtt image exist")
            cmd = [ 'sudo', 'docker', 'images']
            output = subprocess.Popen( cmd, stdout=subprocess.PIPE ).communicate()[0]
            words = []
            lines = output.split('\n')
            for line in lines:
                for word in line.split(' '):
                    tmp = word.replace("\n", "").replace("\t", "").replace(" ", "")
                    if tmp is not "":
                        words.append(word)

            docker_image = config.get("server config", "docker image")
            found_image = False
            for word in words:
                if docker_image in word:
                    docker_image = word
                    found_image = True
                    break;
            if found_image is False:
                logger.warn("there is no mqtt docker image")
                logger.info("downloading the mqtt broker image from docker (this can take 30 minutes)")
                cmd = ['sudo', 'docker', 'pull', 'pascaldevink/rpi-mosquitto']
                logger.info("execute %s"%cmd)
                output = subprocess.Popen( cmd, stdout=subprocess.PIPE ).communicate()[0]
                logger.info("docker images pascaldevink/rpi-mosquitto downloaded")
                logger.info("create & start docker container from docker image '%s'"%docker_image)
                cmd = ['sudo', 'docker', 'run', '-d', '-tip', '1883:1883', '-p', '9001:9001', '--name=mqtt', docker_image]
                output = subprocess.Popen( cmd, stdout=subprocess.PIPE ).communicate()[0]
            else:
                logger.debug("found docker mqtt broker docker image: %s"%docker_image)
                logger.info("create & start docker container from docker image '%s'"%docker_image)
                cmd = ['sudo', 'docker', 'run', '-d', '-tip', '1883:1883', '-p', '9001:9001', '--name=mqtt', docker_image]
                output = subprocess.Popen( cmd, stdout=subprocess.PIPE ).communicate()[0]
                logger.info(output)

        else:
            logger.debug("docker container '%s' exits"%container_name)
            logger.info("start mqtt broker '%s'"%container_name)
            cmd = [ 'sudo', 'docker', 'start', container_name]
            output = subprocess.Popen( cmd, stdout=subprocess.PIPE ).communicate()[0]
    else:
        logger.info("docker mqtt broker '%s' is running"%container_name)


def init():
    config_logger()
    logger.debug("============= start programm =================")
    logger.debug("init mqtt server")
    check_if_root()
    config_file_init()
    init_mqtt()

def config_mqtt():
    global config
    global client
    logger.info("config mqtt client")
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    url = config.get("server config", "url")
    port = config.get("server config", "port")
    logger.info("connect to mqtt broker: %s on port %s"%(url,port))
    client.connect(url, port)

    channels = config.get("server config", "channels")
    logger.debug("load channels %s"%channels)
    json_data = channels
    channels = json.loads(json_data)
    for channel in channels["channels"]:
        logger.info("join channel: %s"%channel)
        client.subscribe(channel)

def set_config_file_path(path):
    global config_file_path
    config_file_path = path

def set_log_file_path(path):
    global log_path
    log_path = path

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='MQTT homeautomatisation server')
    parser.add_argument('--config', '-c', nargs='?', help='set the path of config file', type=set_config_file_path)
    parser.add_argument('--log', '-l', nargs='?', help='set the path of log file', type=set_log_file_path)
    args = parser.parse_args()
    try:
        init()
        config_mqtt()
        client.loop_forever()
        asdf()
    except KeyboardInterrupt:
        logger.error("an KeyboardInterrupt appeared probably from Ctr + C")
        logger.error(sys.exc_info()[0])
        logger.error(sys.exc_info()[1])
        var = traceback.format_exc()
        logger.error(var)
        raise
    except:
        logger.error("a fatal error appeared please check the logs")
        logger.error(sys.exc_info()[0])
        logger.error(sys.exc_info()[1])
        var = traceback.format_exc()
        logger.error(var)
        raise
